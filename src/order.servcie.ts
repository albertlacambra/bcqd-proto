import { Order } from "./order";
import { OrderDao } from "./order.dao";

export class OrderService {

    constructor(private readonly orderDao: OrderDao) {
        console.log("[OrderService] constructed");
    }

    public createOrder(order: Order) {

        return this.orderDao.save(order);

    }

    public listOrders() {
        return this.orderDao.fetchAll();

    }

    public getOrder(orderId: string) {
        return this.orderDao.getOrder(orderId);
    }

}


