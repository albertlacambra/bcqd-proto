import { Order } from "./order";
import { PersistenceService } from "./persistence/mongodb/persistence.service";

export class OrderDao {

    constructor(private readonly persistenceService: PersistenceService) {

    }

    public save(order: Order): Order {
        return this.persistenceService.save(order);
    }

    public fetchAll(): Order[] {
        return this.persistenceService.fetchAll();
    }

    public getOrder(orderId: string) {
        return this.persistenceService.find(orderId);
    }

}