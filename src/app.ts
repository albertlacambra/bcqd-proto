import express from "express";
import * as bodyParser from 'body-parser';
import { CreateOrderInquiryHandler, GetOrderHandler, ListOrdersHandler } from './order.handlers.js';
import { OrderService } from "./order.servcie.js";
import { OrderDao } from "./order.dao.js";
import { PersistenceService } from "./persistence/mongodb/persistence.service.js";

const app = express();
app.use(bodyParser.json({
    limit: '50mb',
    verify(req: any, res, buf, encoding) {
        req.rawBody = buf;
    }
}));

app.use(function (req, res, next) {
    const r: any = req;
    r['cid'] = "abcd-" + Math.random();
    res.set('cid', extractCid(req));
    next();
});

app.get('/', (req, res) => res.send('Hello World!'));
app.get('/say/:toSay', (req, res, next) => {
    console.info("ok1");
    res.append("H1 - ");
    next();
}, (req, res) => {
    console.info("ok2");

    res.send(`Hello1 ${req.params.toSay}`);
});

app.get('/say/albert', (req, res) => {
    console.info("kk");
    req.params['']
    res.send(`Hello mooook`);
});

app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.xhr) {
        res.status(400).send({ error: 'Something failed!' })
    } else {
        next(err)
    }
});


const orderService: OrderService = new OrderService(new OrderDao(new PersistenceService()));

app.get('/api/v1/order/create', (req, res, next) => new CreateOrderInquiryHandler(orderService).handle(req, res, next));
app.get('/api/v1/order/list', (req, res) => new ListOrdersHandler(orderService).handle(req, res));
app.get('/api/v1/order/get/:id', (req, res, next) => new GetOrderHandler(orderService).handle(req, res, next, extractCid(req)));

app.use(function errorHandler(err: any, req: any, res: any, next: any) {
    res.status(510)
    res.set('cid', extractCid('cid'));
    res.json({ code: err.code, message: err.message, cid: extractCid(req) });
});


app.use(function errorHandler(err: any, req: any, res: any, next: any) {
    res.status(510)
    res.set('cid', extractCid('cid'));
    res.json({ code: err.code || "INVALID_SOMETHING", message: err.message, cid: extractCid(req) });
    next();
})

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("response done", extractCid(req));
});

app.listen(3000, '0.0.0.0', () => {
    console.info("started");
});

function extractCid(req: any) {
    return req['cid'];
}