import express from 'express';
import { Order } from './order';
import { OrderService } from './order.servcie';

export class CreateOrderInquiryHandler {

    constructor(private readonly orderService: OrderService) {

    }

    public handle(req: express.Request, res: express.Response, next: express.NextFunction): any {
        const o: Order = this.orderService.createOrder({ id: `O-${new Date().getTime()}`, name: "a random order-" + Math.random() })
        console.log('fine for me', o);
        res.json(o);
        next();
    }

}

export class ListOrdersHandler {

    constructor(private readonly orderService: OrderService) {

    }

    public handle(req: express.Request, res: express.Response, next?: express.NextFunction): any {
        res.send(this.orderService.listOrders());
    }
}

export class GetOrderHandler implements RequestHandler<string> {

    constructor(private readonly orderService: OrderService) {

    }

    public handle(req: express.Request, res: express.Response, next: express.NextFunction, cid: string): any {

        console.log('cid', cid);

        try {
            const r: RequestValidationResult = this.validate(req);


            if (!r.isValid()) {
                throw new Error(JSON.stringify(r.errors));
            }

            const orderId: string = this.extractParams(req);

            res.set('cid', cid);
            res.send(this.orderService.getOrder(orderId));
        } catch (e) {
            // res.status(501)
            // res.json({ code: 'balblabla', error: e.message, cid: cid });
            throw e;
        };
    }

    public validate(req: express.Request): RequestValidationResult {

        if (!req.params['id'] || !req.params['id'].startsWith("O")) {
            return new RequestValidationResult([`id param not given or invalid. id=${req.params['id']}`]);
        }

        return new RequestValidationResult();
    }

    public extractParams(req: express.Request): string {
        return req.params['id'];
    }

}

interface RequestHandler<P> {
    handle(req: express.Request, res: express.Response, next: express.NextFunction, cid: string): any;
    validate(req: express.Request): RequestValidationResult;
    extractParams(req: express.Request): P;
}

export class RequestValidationResult {

    constructor(private readonly _errors?: string[]) { };

    public get errors() {
        return this._errors ? this._errors : []
    }

    public isValid(): boolean {
        return this.errors.length === 0;
    }


}   