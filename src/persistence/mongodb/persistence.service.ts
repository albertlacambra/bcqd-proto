export class PersistenceService {

    private readonly store: Map<any, any>;

    constructor() {
        this.store = new Map<any, any>();
    }

    public save<T extends Entity>(entity: T): T {

        this.store.set(entity.id, entity);
        console.log(`[save] 1 ${JSON.stringify(Array.from(this.store.entries()))}`);
        console.log(`[save] 2 ${JSON.stringify(Array.from(this.store.values()))}`);

        return entity;

    }

    public fetchAll<T extends Entity>(): T[] {
        console.log(`[fetchAll] 1 ${JSON.stringify(Array.from(this.store.entries()))}`);
        return Array.from(this.store.values());
    }

    public find<T extends Entity>(id: any): T[] {
        return this.store.get(id);
    }
}

export interface Entity {
    id: any;
}