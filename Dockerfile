FROM node:12.18.3-alpine3.11

ARG npm_token
RUN echo "@ibm:registry=https://nexus3.cus1.nexplore.com/repository/npm-hosted/" >> $(npm config get userconfig)
RUN echo "//nexus3.cus1.nexplore.com/repository/npm-hosted/:_authToken=${npm_token}" >> $(npm config get userconfig)

RUN apk add --update python make g++ git

RUN mkdir -p /usr/src/logs

# Create app directory
WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm install
COPY . /usr/src/app/
RUN npm run grunt ts

EXPOSE 3000
EXPOSE 2000
CMD [ "npm", "start" ]
